import bpy

sceneKey = bpy.data.scenes.keys()[0]

bpy.ops.import_scene.x3d(filepath="model.x3d")

c=0
for obj in bpy.data.objects:
	if ( obj.type =='CAMERA'):
		print("Rendering scene["+sceneKey+"] with Camera["+obj.name+"]")

		bpy.data.scenes[sceneKey].camera = obj
		bpy.data.scenes[sceneKey].render.filepath = '//camera_' + str(c)
		bpy.data.scenes[sceneKey].render.resolution_x = 640
		bpy.data.scenes[sceneKey].render.resolution_y = 480
		bpy.data.scenes[sceneKey].render.resolution_percentage = 100

		bpy.ops.render.render(write_still=True)

		c=c+1
